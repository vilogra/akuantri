import React, {Component} from "react";
import "./App.scss";
import "./assets/themify-icons/themify-icons.css";
import HeaderAkuantri from "./components/HeaderAkuantri";
import CoverAkuantri from "./components/CoverAkuantri";
import ContentAkuantri from "./components/ContentAkuantri"
import FooterAkuantri from "./components/FooterAkuantri";

class App extends Component {
  render() {
    return (
      <div className="halaman">
        <HeaderAkuantri />
        <CoverAkuantri />
        <ContentAkuantri />
        <FooterAkuantri />
      </div>
    )
  }
}

const warna = {
  color: "#00cec9",
  fontSize: "30px"
};

const lineBottom = {
  borderBottom: "3px solid #494a00"
};

export default App;

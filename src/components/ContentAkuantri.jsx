import React, { Component } from 'react'

export default class ContentAkuantri extends Component {
    render() {
        return (
            <div>
                <div class="section">
                    <div class="flex-container flex-column">
                        <h1><span>Ber</span>bagai Jenis Antrian</h1>
                        <h3>Jangan habiskan waktu Anda hanya untuk mengantri.</h3>
                        <div class="text-padding"></div>
                        <div class="row">
                            <div class="half">
                                <ul class="items">
                                    <li>Layanan Kesehatan<span>Antri di Rumah Sakit, Klinik, dan Layanan kesehatan lain kini bisa lebih menyenangkan.</span></li> 
                                    <li>Berbagai Tiket Box<span>Tidak takut lagi kehabisan Tiket atau Diskon menarik dengan layanan Akuantri.</span></li>
                                    <li>Berbagai Restaurant<span>Jangan lewatkan Diskon atau Opening Restaurant kesukaan Anda, Kami yang akan Antri.</span></li>
                                    <li>Salon atau Barbershop<span>Kami yang akan menunggu giliran Anda di Salon atau Barbershop kesayangan Anda.</span></li>
                                    <li>Layanan Pemerintahan atau Bank<span>Tidak bisa datang pagi untuk Antri? Kami yang akan menggantikan Antrian Anda.</span></li>
                                    <li>Jenis Antrian Lain<span>Antrian tidak masuk kategori? Ceritakan tentang jenis Antrian Anda kepada kami.</span></li>
                                </ul>
                            </div>
                            <div class="half">
                                <img src={require('../assets/images/aplikasi-akuantri-hp1.png')} alt="Aplikasi Akuantri" title="Aplikasi Akuantri"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section faq">
                    <div class="container">
                        <div class="centering">
                            <h1>Frequently <span>Ask</span> Questions</h1>
                            <h3>Pertanyaan yang Sering ditanyakan Tentang Akuantri</h3>
                        </div>
                        <div class="row">
                            <div class="acordion-container right-1">
                                <div class="acordion">
                                    <div class="acordion-head" id="acordhead-1">
                                        <span class="icon">
                                            <i class="ti-plus"></i>
                                        </span>
                                        <h3>Apa & Siapa itu Liner Akuantri</h3>
                                    </div>
                                    <div class="acordion-content" id="acordcontent-1">
                                        <p>Mitra akuantri yang ada di lapangan untuk mengantri/mengantikan posisi antrian Anda.</p>
                                    </div>
                                </div>

                                <div class="acordion">
                                    <div class="acordion-head" id="acordhead-2">
                                            <span class="icon">
                                                <i class="ti-plus"></i>
                                            </span>
                                        <h3>Jasa apa saja yang diberikan Akuantri</h3>
                                    </div>
                                    <div class="acordion-content" id="acordcontent-2">
                                        <p>Jasa Layanan Tunggu dan Antrian diantaranya sebagai berikut :</p><br/>
                                        <ul>
                                            <li>Jasa Antri di restaurant meliputi layanan tunggu pada antrian panjang atau lama, layanan pemesanan Table.</li>
                                            <li>Antrian dilokasi-lokasi layanan kesehatan seperti Klinik, Rumah Sakit, Rumah Rumah Sakit Bersalin, Bersalin, Tempat Terapi Kesehatan dan sebagainya.</li>
                                            <li>Antrian pada tempat-tempat atau acara hiburan seperti diloket pembelian tiket bioskop, tiket konser, tiket travel fair dan sebagainya.</li>
                                            <li>Antrian lama di layanan kecantikan seperti Salon ataupun Barbershop.</li>
                                            <li>Antrian pada Customer Service Perbankan.</li>
                                            <li>Antrian ditempat kepengurusan Pajak Kendaraan juga instansi sejenis lainnya.</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="acordion">
                                    <div class="acordion-head" id="acordhead-3">
                                            <span class="icon">
                                                <i class="ti-plus"></i>
                                            </span>
                                        <h3>Bagaimana cara pemesanan Liner Akuantri?</h3>
                                    </div>
                                    <div class="acordion-content" id="acordcontent-3">
                                        <p>Sangat mudah, silahkan Download Aplikasi Akuantri di Google Play. Setelah itu klik Icon Antrian apa yang Anda butuhkan, isi data seperti lokasi antrian, kapan tanggal akan dilakukan antrian, Jam untuk mulai mengantri, pesan tambahan atau informasi tambahan tentang antrian Anda.</p>
                                        <p style={warnaMerah}> Catetan : Pemesanan Minimal 7 jam sebelum waktu antrian </p>
                                        <p>Konfirmasi pembayaran hanya dilakukan pada jam kerja Akuantri (09.00 - 17.00)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="acordion-container left-1">
                                <div class="acordion">
                                    <div class="acordion-head" id="acordhead-4">
                                            <span class="icon">
                                                <i class="ti-plus"></i>
                                            </span>
                                        <h3>Bagaimana cara membayar layanan Akuantri?</h3>
                                    </div>
                                    <div class="acordion-content" id="acordcontent-4">
                                        <p>Setelah Anda berhasil melakukan Pesanan Antrian maka Anda akan menerima Email yang menjelaskan bagaimana cara pembayaran dan jumlah yang harus dibayar sesuai dengan estimasi Antrian yang Anda pesan. Jika waktu Antrian melebihi estimasi Antrian yang Anda pesan, maka akan berlaku tarif dasar dan pembayaran dilakukan secara langsung kepada Liner yang bertugas. </p>
                                    </div>
                                </div>

                                <div class="acordion">
                                    <div class="acordion-head" id="acordhead-5">
                                            <span class="icon">
                                                <i class="ti-plus"></i>
                                            </span>
                                        <h3>Berapa tarif dasar layanan Akuantri?</h3>
                                    </div>
                                    <div class="acordion-content" id="acordcontent-5">
                                        <p>Tarif dasar Layanan Kami adalah Rp. 2500 untuk setiap 5 Menit.</p>
                                        <p style={warnaMerah}>Untuk pemesanan durasi antrian 3 Jam atau lebih, maka tarif dasar kami adalah 50% dari total estimasi harga durasi antrian yang Anda pesan.</p>
                                    </div>
                                </div>

                                <div class="acordion">
                                    <div class="acordion-head" id="acordhead-6">
                                            <span class="icon">
                                                <i class="ti-plus"></i>
                                            </span>
                                        <h3>Berapa jumlah Liner yg dapat dipesan?</h3>
                                    </div>
                                    <div class="acordion-content" id="acordcontent-6">
                                        <p>1 Order via Aplikasi bisa melakukan pemesanan untuk lebih dari 1 Liner, dengan ketentuan 1 Liner hanya dapat melayani 1 Pelanggan atau 1 Antrian.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="section testimonial">
                    <div class="container">
                        <div class="centering">
                            <h1>Test<span>imo</span>nial</h1>
                            <h3>Mereka yang telah menghemat waktunya untuk mengantri.</h3>
                            <br/>
                        </div>
                        <div class="flex-container flex-center-around">
                            <div class="item">
                                <img src={require('../assets/images/testi-akuantri-1.png')} alt="Layanan Jasa Antri Terpercaya, Akuantri"/>
                            </div>
                            <div class="item">
                                <img src={require('../assets/images/testi-akuantri-2.png')} alt="Layanan Jasa Antri Terpercaya, Akuantri"/>
                            </div>
                            <div class="item">
                                <img src={require('../assets/images/testi-akuantri-3.png')} alt="Layanan Jasa Antri Terpercaya, Akuantri"/>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="section blog">
                    <div class="container">
                        <div class="flex-container flex-center-around centering">
                            <div class="item-blog">
                                <span class="icon">
                                    <i class="ti-timer"></i>
                                </span>
                                <h3>Hemat Waktu Anda</h3><br/>
                                <p>Bebaskan dan hemat waktu Anda untuk Antri, Kami yang akan menggantikan posisi Antrian panjang Anda. </p>
                            </div>
                            <div class="item-blog">
                                <span class="icon">
                                    <i class="ti-calendar"></i>
                                </span>
                                <h3>Pesan Antrian Kapan Saja</h3><br/>
                                <p>Silahkan pesan Antrian untuk sekarang , Besok, Minggu depan, kapan Saja. Liner kami akan siap Antri untuk Anda. </p>
                            </div>                 
                            <div class="item-blog">
                                <span class="icon">
                                    <i class="ti-ticket"></i>
                                </span>
                                <h3>Berbagai Jenis Antrian</h3><br/>
                                <p>Beragam jenis Antrian tinggal pesan seperti Antri BPJS, Imigrasi, Pembayaran pajak, dan Antrian lainya. </p>     
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const warnaMerah = {
    color: `red`
}

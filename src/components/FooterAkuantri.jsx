import React, { Component } from 'react'
import PlayStore from '../assets/images/playstore.png'

export default class FooterAkuantri extends Component {
    render() {
        return (
            <footer className="footer">
                <div className="container">
                    <div className="flex-container flex-around">
                        <div className="item-footer-1">
                            <h3>Sekilas Akuantri</h3><br/>
                            <p>
                                <a href="https://www.akuantri.com">Akuantri</a> adalah Aplikasi Layaan Jasa Antri yang akan menghemat waktu Anda dalam hal mengantri. Kami mempunyai jenis Antrian yang beragam. dengan <a href="https://www.akuantri.com">Akuantri</a> Anda bisa melakukan hal yang lebih penting, sementara antrian Anda akan tetap berajalan.
                            </p>
                        </div>
                        <div className="item-footer-2">
                            <h3>Links</h3><br/>
                            <ul>
                                <li><a href="#">Tentang Kami</a></li>
                                <li><a href="#">Promo</a></li>
                                <li><a href="#">Karir</a></li>
                                <li><a href="#">Hubungi Kami</a></li>
                                <li><a href="#">Syarat & Ketentuan</a></li>
                            </ul>
                        </div>
                        <div className="item-footer-3">
                            <h3>Download Aplikasi</h3><br/>
                            <div className="btn-big">
                                <a href="#download"> <img src={PlayStore} width="210" alt="Download Aplikasi Akuantri" title="Download Aplikasi Akuantri"/></a>
                            </div>
                        </div>
                    </div><hr/>
                    <div className="flex-container">
                        <div className="row copyright">
                            <div className="col-md-6">
                                <p>
                                    <small class="block">&copy; <span><a href="#">PT Aku Antri Indonesia</a></span>. All Rights Reserved.</small>
                                    <small class="block">Jasa Layanan Antri Indonesia - Akuantri</small>
                                </p>
                            </div>
                            <ul>
                                <li><a href="https://twitter.com/akuantriID"><i class="ti-twitter-alt"></i></a></li>
                                <li><a href="https://www.facebook.com/akuantriID"><i class="ti-facebook"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UCRv54xIoKp90p2UO5tDn4hA"><i class="ti-youtube"></i></a></li>
                                <li><a href="https://www.instagram.com/akuantri/"><i class="ti-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

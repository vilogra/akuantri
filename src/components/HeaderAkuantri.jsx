import React, { Component } from 'react'
import Logo from '../assets/images/logo-akuantri.png'

export default class HeaderAkuantri extends Component {
    render() {
        return (
            <div className="nav">
                <div className="flex-container">
                    <div className="logo pad-t12 pad-l10">
                            <img src={Logo} alt="Logo Akuantri" width="180"/>
                    </div>
                    <div className="fa-left pad-t25 pad-r30">
                        <ul>
                            <li className="active"><a href="#home">Home</a></li>
                            <li><a href="#layanan">Layanan</a></li>
                            <li><a href="#faq">FAQ</a></li>
                            <li><a href="#blog">Blog</a></li>
                            <li><a href="#about">Tentang Kami</a></li>
                            <li className="btn-cta"><a href="#daftar"><span>Daftar Liner</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

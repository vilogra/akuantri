import React, { Component } from 'react'
import PlayStore from '../assets/images/playstore.png'
import Pelajari from '../assets/images/pelajari2.png'
import Cover from '../assets/images/bg-akuantri-home1.jpg'

export default class CoverAkuantri extends Component {
    render() {
        return (
        <div>
            <div class="nav-dist"></div>
            <div class="cover" style={backgroundCover}>
                <div class="overlay"></div>
                <div class="text-container">
                    <h1><strong>Akuantri Indonesia</strong></h1>
                    <p align="center" class="font-white">Kami sadar menunggu adalah hal yang membosankan, dan kami juga sadar akan ada banyak waktu terbuang <br/>begitu saja karna harus menunggu. Untuk itu, <b>Akuantri</b> hadir untuk membuat waktu berharga Anda<br/>tidak hilang begitu saja. Kami akan menggantikan antrian Anda, karna kami sadar bahwa <br/><b>Waktu Anda sangat Berharga.</b></p><br/>
                    <div class="col-md-6 middle top-2">
                        <div class="btn-big fleft pad-right-2">
                            <img src={PlayStore} alt="Download Aplikasi" title="Download Aplikasi Akuantri" />
                        </div>
                        <div class="btn-big fleft">
                            <img src={Pelajari} alt="Pelajari Akuantri" title="Download Aplikasi Akuantri" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

const backgroundCover = {
    backgroundImage: `url(${Cover})`
}
